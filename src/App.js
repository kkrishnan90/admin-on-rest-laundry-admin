import React, { Component } from 'react';
import './App.css';

import { jsonServerRestClient, Admin, Resource, LogOut } from 'admin-on-rest';
import addUploadCapabilities from './addUploadCapabilities';
import { UserList, UserEdit, UserCreate } from './components/users';
import {
  DistrictList,
  DistrictEdit,
  DistrictCreate
} from './components/districts';
import authClient from './authClient';
import { ChannelList, ChannelEdit, ChannelCreate } from './components/channels';
import { AdList, AdEdit, AdCreate } from './components/advertisements';
import {
  DistrictUserList,
  DistrictUserEdit,
  DistrictUserCreate
} from './components/district-user';
import { StatesList, StatesEdit, StatesCreate } from './components/states';
import {
  StatesDistrictList,
  StatesDistrictEdit,
  StatesDistrictCreate
} from './components/states-districts';

const restClient = jsonServerRestClient('http://139.59.31.35:5000');
const uploadCapableClient = addUploadCapabilities(restClient);

const resolveEditAccess = ({ resource, permissions, exact, value }) => {
  // value = the requested permissions specified in the `permissions` prop (eg `admin`). May be undefined
  // resource = the requested resource (eg `posts`)
  // exact = the value of the `exact` prop
  console.log('Edit Access Check');
  // permissions = the result of the authClient call
};

class App extends Component {
  render() {
    return (
      <Admin
        title='TV App Admin'
        authClient={authClient}
        restClient={uploadCapableClient}>
        <Resource
          name='users'
          options={{ label: 'Users' }}
          list={UserList}
          edit={UserEdit}
          create={UserCreate}
        />
        <Resource
          name='states'
          options={{ label: 'States' }}
          list={StatesList}
          edit={StatesEdit}
          create={StatesCreate}
        />
        <Resource
          name='state-district'
          options={{ label: 'State-District' }}
          list={StatesDistrictList}
          edit={StatesDistrictEdit}
          create={StatesDistrictCreate}
        />
        <Resource
          name='districts'
          options={{ label: 'Districts' }}
          list={DistrictList}
          edit={DistrictEdit}
          create={DistrictCreate}
        />
        <Resource
          name='user-state'
          options={{ label: 'User-State' }}
          list={DistrictUserList}
          edit={DistrictUserEdit}
          create={DistrictUserCreate}
        />
        <Resource
          name='channels'
          options={{ label: 'Channels' }}
          list={ChannelList}
          edit={ChannelEdit}
          create={ChannelCreate}
        />
        <Resource
          name='ads'
          options={{ label: 'Advertisements' }}
          list={AdList}
          edit={AdEdit}
          create={AdCreate}
        />
      </Admin>
    );
  }
}

export default App;
