import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  ImageInput,
  Edit,
  Create,
  ImageField,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

export const ChannelList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />
      <TextField label='Channel Name' source='name' />
      <ImageField
        style={{ height: 50, width: 50 }}
        source='pictures[0].src'
        label='Channel Image'
        title='Channel Image'
      />
      <ReferenceField label='District' source='district' reference='districts'>
        <TextField source='name' />
      </ReferenceField>

      <EditButton />
    </Datagrid>
  </List>
);

export const ChannelEdit = (props) => (
  <Edit title={'Edit Channel'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <ReferenceInput
        label='District'
        source='district'
        reference='districts'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
      <ImageInput source='pictures' label='Channel Logo here' accept='image/*'>
        <ImageField source='src' />
      </ImageInput>

      <TextInput label='Channel Name' source='name' />
      <TextInput label='Description' source='description' />
      <TextInput label='Channel Link' source='url' />
    </SimpleForm>
  </Edit>
);

export const ChannelCreate = (props) => (
  <Create title={'Create Channel'} {...props} redirect='show'>
    <SimpleForm>
      <ReferenceInput
        label='District'
        source='district'
        reference='districts'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
      <ImageInput source='pictures' label='Channel Logo here' accept='image/*'>
        <ImageField source='src' />
      </ImageInput>
      <TextInput label='Channel Name' source='name' />
      <TextInput label='Description' source='description' />
      <TextInput label='Channel Link' source='url' />
    </SimpleForm>
  </Create>
);
