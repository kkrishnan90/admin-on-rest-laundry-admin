import React from 'react';
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

export const UserList = (props) => (
  <List {...props} redirect='show'>
    <Datagrid>
      <TextField source='id' />
      <TextField source='username' />
      <EditButton />
    </Datagrid>
  </List>
);

export const UserEdit = (props) => (
  <Edit title={'Edit Users'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput source='id' />

      <TextInput source='username' />
      <TextInput source='password' />
    </SimpleForm>
  </Edit>
);

export const UserCreate = (props) => (
  <Create title={'Create Users'} {...props} redirect='show'>
    <SimpleForm>
      <TextInput source='username' />
      <TextInput source='password' />
    </SimpleForm>
  </Create>
);
