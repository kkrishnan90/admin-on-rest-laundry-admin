import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  ImageInput,
  Edit,
  Create,
  ImageField,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

export const AdList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />
      <ImageField
        style={{ height: 50, width: 50 }}
        source='pictures[0].src'
        label='Advertisement Image'
        title='Advertisement Image'
      />

      <EditButton />
    </Datagrid>
  </List>
);

export const AdEdit = (props) => (
  <Edit title={'Edit Channel'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <ImageInput
        source='pictures'
        label='Advertisement Image'
        accept='image/*'>
        <ImageField source='src' />
      </ImageInput>
    </SimpleForm>
  </Edit>
);

export const AdCreate = (props) => (
  <Create title={'Create Channel'} {...props} redirect='show'>
    <SimpleForm>
      <ImageInput
        source='pictures'
        label='Advertisement Image'
        accept='image/*'>
        <ImageField source='src' />
      </ImageInput>
    </SimpleForm>
  </Create>
);
