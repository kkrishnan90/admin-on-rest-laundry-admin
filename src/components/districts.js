import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

const imageStyle = {
  maxHeight: '4rem',
  margin: '0.5rem'
};
export const DistrictList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />

      <TextField source='name' />
      <EditButton />
    </Datagrid>
  </List>
);

export const DistrictEdit = (props) => (
  <Edit title={'Edit District'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <TextInput label='District Name' source='name' />
    </SimpleForm>
  </Edit>
);

export const DistrictCreate = (props) => (
  <Create title={'Create District'} {...props} redirect='show'>
    <SimpleForm>
      <TextInput label='District Name' source='name' />
    </SimpleForm>
  </Create>
);
