import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

const imageStyle = {
  maxHeight: '4rem',
  margin: '0.5rem'
};
export const StatesList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />

      <TextField source='name' />
      <EditButton />
    </Datagrid>
  </List>
);

export const StatesEdit = (props) => (
  <Edit title={'Edit State'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <TextInput label='State Name' source='name' />
    </SimpleForm>
  </Edit>
);

export const StatesCreate = (props) => (
  <Create title={'Create State'} {...props} redirect='show'>
    <SimpleForm>
      <TextInput label='State Name' source='name' />
    </SimpleForm>
  </Create>
);
