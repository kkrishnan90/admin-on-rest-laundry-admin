import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

const imageStyle = {
  maxHeight: '4rem',
  margin: '0.5rem'
};
export const StatesDistrictList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />
      <ReferenceField label='State' source='state' reference='states'>
        <TextField source='name' />
      </ReferenceField>

      <ReferenceField label='District' source='district' reference='districts'>
        <TextField source='name' />
      </ReferenceField>
      <EditButton />
    </Datagrid>
  </List>
);

export const StatesDistrictEdit = (props) => (
  <Edit title={'Edit State-District'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <ReferenceInput
        label='State'
        source='state'
        reference='states'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>

      <ReferenceInput
        label='District'
        source='district'
        reference='districts'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const StatesDistrictCreate = (props) => (
  <Create title={'Create State-District'} {...props} redirect='show'>
    <SimpleForm>
      <ReferenceInput
        label='State'
        source='state'
        reference='states'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>

      <ReferenceInput
        label='District'
        source='district'
        reference='districts'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);
