import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  DisabledInput,
  TextInput
} from 'admin-on-rest';

const imageStyle = {
  maxHeight: '4rem',
  margin: '0.5rem'
};
export const DistrictUserList = (props) => (
  <List {...props} redirect='list'>
    <Datagrid>
      <TextField source='id' />
      <ReferenceField label='User' source='user' reference='users'>
        <TextField source='username' />
      </ReferenceField>

      <ReferenceField label='State' source='state' reference='states'>
        <TextField source='name' />
      </ReferenceField>
      <EditButton />
    </Datagrid>
  </List>
);

export const DistrictUserEdit = (props) => (
  <Edit title={'Edit District'} {...props} redirect='show'>
    <SimpleForm>
      <DisabledInput disabled source='id' />

      <ReferenceInput label='User' source='user' reference='users' allowEmpty>
        <SelectInput optionText='username' />
      </ReferenceInput>

      <ReferenceInput
        label='State'
        source='state'
        reference='states'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const DistrictUserCreate = (props) => (
  <Create title={'Create District'} {...props} redirect='show'>
    <SimpleForm>
      <ReferenceInput label='User' source='user' reference='users' allowEmpty>
        <SelectInput optionText='username' />
      </ReferenceInput>

      <ReferenceInput
        label='State'
        source='state'
        reference='states'
        allowEmpty>
        <SelectInput optionText='name' />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);
