import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK } from 'admin-on-rest';

export default (type, params) => {
  if (type === AUTH_LOGIN) {
    const { username, password } = params;
    const request = new Request('http://139.59.31.35:5000/admin/login', {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: new Headers({ 'Content-Type': 'application/json' })
    });
    return fetch(request).then((response) => {
      if (response.status < 200 || response.status >= 300) {
        throw new Error(response.statusText);
      } else
        localStorage.setItem('sid', response.headers.get('X-Session-Token'));
      return response.json();
    });
  }
  if (type === AUTH_CHECK) {
    return localStorage.getItem('sid') ? Promise.resolve() : Promise.reject();
  }
  if (type === AUTH_LOGOUT) {
    localStorage.removeItem('sid');
    return Promise.resolve();
  }

  return Promise.reject('Unkown method');
};
